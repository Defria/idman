using System;
using IdMan.API.Validators;
using NUnit.Framework;

namespace IdMan.API.Tests.Validators
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            Validator = new RSAIDValidator();
        }

        private RSAIDValidator Validator { get; set; }

        // [TestCase("1234567891111")]
        [TestCase("123456789")]
        public void Validate_WhenCalledWithInvalidIdNumber_ThrowsArgumentOutOfRangeExceptionException(string idNumber)
        {
            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => Validator.Validate(idNumber));
        }

        [Test]
        public void Validate_WhenCalledWithInvalidIdNumber_ThrowsArgumentExceptionException()
        {
            // Assert
            Assert.Throws<Exception>(() => Validator.Validate("1234567891111"));
        }


        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetCitizenshipCitizen()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "Citizen";

            // Act
            var result = validator.Validate("4205155187082");

            // Assert
            Assert.True(string.Equals(result.Citizenship, expectedResult, StringComparison.InvariantCultureIgnoreCase),
                $"expected value {expectedResult}, actual {result.Citizenship}");
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetCitizenshipResident()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "Resident";

            // Act
            var result = validator.Validate("8801234800186");

            // Assert
            Assert.True(
                string.Equals(result.Citizenship, expectedResult, StringComparison.InvariantCultureIgnoreCase),
                $"our value {expectedResult}, actual {result.Gender}");
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetDateOfBirth()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "880123";

            // Act
            var result = validator.Validate("8801235111088");

            // Assert
            Assert.AreEqual(result.DateOfBirth, expectedResult);
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetGenderFemale()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "Female";

            // Act
            var result = validator.Validate("8801234800087");

            // Assert
            Assert.True(string.Equals(result.Gender, expectedResult, StringComparison.InvariantCultureIgnoreCase),
                $"our value {expectedResult}, actual {result.Gender}");
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetGenderMale()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "Male";

            // Act
            var result = validator.Validate("8801235111088");

            // Assert
            Assert.True(string.Equals(result.Gender, expectedResult, StringComparison.InvariantCultureIgnoreCase));
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetIdNumber()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "8801235111088";

            // Act
            var result = validator.Validate("8801235111088");

            // Assert
            Assert.AreEqual(result.IdNumber, expectedResult);
        }


        [Test]
        public void Validate_WhenCalledWithValidIdNumberWithWhiteSpaces_ShouldReturnIdentity()
        {
            // Arrange
            var idNumber = "8	8	0	1	2	3         	5	1	1	1	0	8	8";


            // Act
            var result = Validator.Validate(idNumber);

            // Assert
            var expectedResult = "8801235111088";
            Assert.IsTrue(result.IdNumber.Equals(expectedResult));
            Assert.IsNotNull(result);
        }
    }
}