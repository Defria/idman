using IdMan.API.Models;

namespace IdMan.API.Validators
{
    public interface IRSAIDValidator
    {
        Identity Validate(string idNumber);
    }
}