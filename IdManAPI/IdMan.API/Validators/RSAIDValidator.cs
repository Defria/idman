using System;
using System.Linq;
using System.Numerics;
using System.Text.RegularExpressions;
using IdMan.API.Models;

namespace IdMan.API.Validators
{
    public class RSAIDValidator : IRSAIDValidator
    {
        public Identity Validate(string idNumber)
        {
            idNumber = RemoveWhiteSpaces(idNumber);
            PreConditionCheck(idNumber);
            var controlDigit = ApplyRules(idNumber);
            PostConditionCheck(idNumber, controlDigit);
            return CreateIdentity(idNumber);
        }

        private int ApplyRules(string idNumber)
        {
            const int indexOfLastDigit = 12;
            var digits = idNumber.Select(s => int.Parse(s.ToString())).ToArray();
            var sumDigitsInOddIndexPositionExcludingLast = digits.Where((_, index) => index % 2 == 0 && index < indexOfLastDigit).Sum();
            var evenPositionedDigitsConcatenated = string.Join(string.Empty, digits.Where((_, index) => index % 2 != 0));
            var productOfEvenPositionedDigits = (int.Parse(evenPositionedDigitsConcatenated) * 2).ToString();
            var sumOfProductEvenPositionedDigits = productOfEvenPositionedDigits.ToCharArray().Sum(c => int.Parse(c.ToString()));
            var controlDigit = CalculateControlDigit(sumDigitsInOddIndexPositionExcludingLast, sumOfProductEvenPositionedDigits);
            return controlDigit;
        }

        private void PostConditionCheck(string idNumber, int controlDigit)
        {
            var result = idNumber.ToCharArray().Select(c => int.Parse(c.ToString())).ToArray();
            if (result.Last() != controlDigit) throw new Exception($"ID number {idNumber} is not valid");
        }

        private void PreConditionCheck(string idNumber)
        {
            const int maxIdNumberLength = 13;
            if (idNumber.Length != maxIdNumberLength)
                throw new ArgumentOutOfRangeException(nameof(idNumber), "ID Number must be exactly 13 digits");

            if (!BigInteger.TryParse(idNumber, out _)) throw new FormatException("ID Number must be only digits");
        }

        private int CalculateControlDigit(int sumResultAllDigitInOddIndexPosition,
            int sumResultOfProductEvenPositionedNumbers)
        {
            const int controlDigitBaseValue = 10;
            var controlDigit = (sumResultAllDigitInOddIndexPosition + sumResultOfProductEvenPositionedNumbers)
                .ToString().Substring(1);
            return controlDigitBaseValue - int.Parse(controlDigit);
        }


        private Identity CreateIdentity(string idNumber)
        {
            var result = idNumber.ToCharArray().Select(c => int.Parse(c.ToString())).ToArray();
            var dateOfBirth = idNumber.Substring(0, 6);
            var gender = result[6] < 5 ? "Female" : "Male";
            var citizenship = result[10] == 0 ? "Citizen" : "Resident";

            var identity = new Identity
            {
                IdNumber = idNumber, DateOfBirth = dateOfBirth, Gender = gender, Citizenship = citizenship
            };
            return identity;
        }

        private void AddEachNumberToDigitList(string idNumber, int[] digitList, int i)
        {
            digitList[i] = int.Parse(idNumber.Substring(i, 1));
        }

        private static string RemoveWhiteSpaces(string idNumber)
        {
            return Regex.Replace(idNumber, @"\s+", "");
        }
    }
}