import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { IdentityComponent } from './identity.component';
import { Identity } from '../models/identity';
import { ReactiveFormsModule } from '@angular/forms';


describe('IdentityComponent', () => {
    let component: IdentityComponent;
    let fixture: ComponentFixture<IdentityComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ReactiveFormsModule, HttpClientTestingModule],
            declarations: [IdentityComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IdentityComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('form invalid when empty', () => {
        expect(component.identityForm.valid).toBeFalsy();
    });

    it('ID number field should be invalid', () => {
      const idNumber = component.identityForm.controls['idNumber'];
      expect(idNumber.valid).toBeFalsy();

      let errors = {};
      errors = idNumber.errors || {};
      expect(errors['required']).toBeTruthy();

      idNumber.setValue("1");
      errors = {};
      errors = idNumber.errors || {};
      expect(errors['minlength']).toBeTruthy();

      idNumber.setValue("12345678901234");
      errors = {};
      errors = idNumber.errors || {};
      expect(errors['maxlength']).toBeTruthy();

      idNumber.setValue('123abc123abc');
      errors = {};
      const identityFrom = component.identityForm;
      errors = identityFrom.errors || {};
      expect(errors['notanumber']).toBeTruthy();

    });

    it('ID number field is valid', () => {
        expect(component.identityForm.valid).toBeFalsy();
        component.identityForm.controls['idNumber'].setValue('1234567890123');
        expect(component.identityForm.valid).toBeTruthy();

        let identity: Identity;
        component.validatedId.subscribe((value) => identity = value);

        component.validateIdentity();

        expect(String(identity.idNumber)).toBe('1234567890123');
    });
});
